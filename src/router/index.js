import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/cabinet/main/",
  },
  {
    path: "/cabinet/",
    redirect: "/cabinet/main/",
  },
  {
    name: "Auth",
    path: "/auth/",
    component: () => import(/* webpackChunkName: "auth" */ "../views/Auth.vue"),
  },
  {
    path: "/cabinet/",
    name: "Cabinet",
    component: () => import(/* webpackChunkName: "cabinet" */ "../views/Cabinet.vue"),
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "/cabinet/main/",
        name: "MainPage",
        component: () => import(/* webpackChunkName: "cabinet" */ "../views/Main.vue"),
      },
      {
        path: "/cabinet/crm/",
        name: "CRM",
        component: () => import(/* webpackChunkName: "cabinet" */ "../views/CRM.vue"),
      },
      {
        path: "/cabinet/crm/contacts/",
        name: "Contacts",
        component: () => import(/* webpackChunkName: "cabinet" */ "../views/Contacts.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters.isAuthorized) {
      next();
      return;
    }
    next("/auth/");
  } else {
    next();
  }
});

export default router;
