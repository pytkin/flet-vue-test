import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const tokenData = {
  token: "0YLQtdGB0YLQvtCy0YvQuSDQv9C+0LvRjNC30L7QstCw0YLQtdC70Yw=",
  name: "Test User",
};

export default new Vuex.Store({
  state: {
    token: localStorage.getItem("token") || null,
    name: localStorage.getItem("name") || null,

    // Contacts
    contacts: [
      {
        name: "Василий Петров",
        phone: "2345678765434",
      },
      {
        name: "Василий Дмитриев",
        phone: "456777777333",
      },
      {
        name: "Иван Иванов",
        phone: "345676543456",
      },
      {
        name: "Екатерина Николаевна",
        phone: "5678765434567",
      },
      {
        name: "Ильдар Исхатович",
        phone: "345676543235",
      },
      {
        name: "Николай Вернандович",
        phone: "3456787654345",
      },
    ],
  },
  actions: {
    authorizeUser(state, payload) {
      const { email, password } = payload;
      console.log(`User authorized with email - "${email}", and password - "${password}"`);

      return new Promise((resolve) => {
        resolve(state.commit("setUserData", tokenData));
      });
    },
  },
  mutations: {
    setUserData(state, payload) {
      const { token, name } = payload;

      state.token = token;
      state.name = name;

      localStorage.setItem("token", token);
      localStorage.setItem("name", name);
    },
    logout(state) {
      state.token = null;
      state.name = null;

      localStorage.setItem("token", null);
      localStorage.setItem("name", null);
    },
  },
  getters: {
    isAuthorized: (state) => typeof state.token === "string",
    getContactItems: (state) => (filters) => {
      let { name, phone } = filters;
      const checkName = typeof name === "string" && name.length > 0;
      const checkPhone = typeof phone === "string" && phone.length > 0;

      if (checkName) name = name.toLowerCase().trim();
      if (checkPhone) phone = phone.toLowerCase().trim();

      return state.contacts.filter((item) => {
        if (checkName && checkPhone) {
          return item.name.toLowerCase().includes(name) && item.phone.toLowerCase().includes(phone);
        } else if (checkName === false && checkPhone) {
          return item.phone.toLowerCase().includes(phone);
        } else if (checkName && checkPhone === false) {
          return item.name.toLowerCase().includes(name);
        }
        return item;
      });
    },
  },
});
